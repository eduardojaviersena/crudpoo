<?php
include_once 'Conexion.php';

class marca
{
    var $objetos;
    public function __construct()
    {
        $db = new Conexion();
        $this->acceso = $db->pdo;
    }

    function crear($nombre) {
        $sql = "SELECT id_marca FROM marca where nombre=:nombre";
        $query = $this->acceso->prepare($sql);
        $query->execute(array(':nombre'=>$nombre));
        $this->objetos = $query->fetchall();
        if(!empty($this->objetos)) {
            echo 'noadd';
        }
        else{
            $sql = "INSERT INTO marca(nombre) values (:nombre);";
            $query = $this->acceso->prepare($sql);
            $query->execute(array(':nombre'=>$nombre));
            echo 'add';
        }

    }

    function buscar(){
        if(!empty($_POST['consulta'])) {
            $consulta=$_POST['consulta'];
            $sql = "SELECT * FROM marca  where nombre LIKE :consulta";
            $query = $this->acceso->prepare($sql);
            $query->execute(array(':consulta' => "%$consulta%"));
            $this->objetos = $query->fetchall();
            return $this->objetos;
            
        }else {
            $sql = "SELECT * FROM marca  where nombre NOT LIKE '' ORDER BY id_marca LIKE 25";
            $query = $this->acceso->prepare($sql);
            $query->execute();
            $this->objetos = $query->fetchall();
            return $this->objetos;
        }   
    }




    function borrar($id) {
            $sql = "DELETE FROM marca where id_marca=:id";
            $query = $this->acceso->prepare($sql);
            $query->execute(array(':id' =>$id));
            if(!empty($query->execute(array(':id' =>$id)))){
               echo 'borrado'; 
            }
            else{
                echo 'noborrado';
            }
    }

    function editar($nombre,$id_editado)
    {
        $sql = "UPDATE marca SET nombre=:nombre where id_marca=:id";
        $query = $this->acceso->prepare($sql);
        $query->execute(array(':id' => $id_editado,':nombre' => $nombre ));
        echo 'edit';
    }

    function rellenar_marcas(){
        $sql = "SELECT * FROM marca order by nombre asc";
        $query = $this->acceso->prepare($sql);
        $query->execute();
        $this->objetos = $query->fetchall();
        return $this->objetos;
    }
}

?>