<?php
include_once 'Conexion.php';

class producto
{
    var $objetos;
    public function __construct()
    {
        $db = new Conexion();
        $this->acceso = $db->pdo;
    }

    function crear($nombre,$concentracion,$adicional,$precio,$marca) {
        $sql = "SELECT id_producto FROM producto where nombre=:nombre and concentracion=:concentracion and adicional=:adicional and id_marca=:marca";
        $query = $this->acceso->prepare($sql);
        $query->execute(array(':nombre'=>$nombre,':concentracion'=>$concentracion,':adicional'=>$adicional,':marca'=>$marca));
        $this->objetos = $query->fetchall();
        if(!empty($this->objetos)) {
            echo 'noadd';
        }
        else{
            $sql = "INSERT INTO producto(nombre,concentracion,adicional,precio,id_marca) values (:nombre,:concentracion,:adicional,:precio,:marca);";
            $query = $this->acceso->prepare($sql);
            $query->execute(array(':nombre'=>$nombre,':concentracion'=>$concentracion,':adicional'=>$adicional,':precio'=>$precio,':marca'=>$marca));
            echo 'add';
        }

    }

    function editar($id,$nombre,$concentracion,$adicional,$precio,$marca) {
        $sql = "SELECT id_producto FROM producto where id_producto!=:id and nombre=:nombre and concentracion=:concentracion and adicional=:adicional and id_marca=:marca";
        $query = $this->acceso->prepare($sql);
        $query->execute(array(':id'=>$id,':nombre'=>$nombre,':concentracion'=>$concentracion,':adicional'=>$adicional,':marca'=>$marca));
        $this->objetos = $query->fetchall();
        if(!empty($this->objetos)) {
            echo 'noedit';
        }
        else{
            $sql = "UPDATE producto SET nombre=:nombre, concentracion=:concentracion, adicional=:adicional, id_marca=:marca, precio=:precio where id_producto=:id";
            $query = $this->acceso->prepare($sql);
            $query->execute(array(':id'=>$id,':nombre'=>$nombre,':concentracion'=>$concentracion,':adicional'=>$adicional,':precio'=>$precio,':marca'=>$marca));
            echo 'edit';
        }

    }

    function buscar(){
        if(!empty($_POST['consulta'])) {
            $consulta=$_POST['consulta'];
            $sql = "SELECT id_producto, producto.nombre as nombre, concentracion, adicional, precio, marca.nombre as marca, producto.id_marca as prod_marca
            FROM producto
            join marca on producto.id_marca=marca.id_marca
            and producto.nombre LIKE :consulta limit 25";
            $query = $this->acceso->prepare($sql);
            $query->execute(array(':consulta' => "%$consulta%"));
            $this->objetos = $query->fetchall();
            return $this->objetos;
            
        }else {
            $sql = "SELECT id_producto, producto.nombre as nombre, concentracion, adicional, precio, marca.nombre as marca, producto.id_marca as prod_marca
           
            FROM producto
            join marca on producto.id_marca=marca.id_marca
            and producto.nombre NOT LIKE '' order by producto.nombre  limit 25";
            $query = $this->acceso->prepare($sql);
            $query->execute();
            $this->objetos = $query->fetchall();
            return $this->objetos;
        }   
    }



    function borrar($id) {
        $sql = "DELETE FROM producto where id_producto=:id";
        $query = $this->acceso->prepare($sql);
        $query->execute(array(':id' =>$id));
        if(!empty($query->execute(array(':id' =>$id)))){
           echo 'borrado'; 
        }
        else{
            echo 'noborrado';
        }
    }




}
?>
