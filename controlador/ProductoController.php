<?php
include '../modelo/Producto.php';
$producto = new producto();
if ($_POST['funcion'] == 'crear') {
    $nombre = $_POST['nombre'];
    $concentracion = $_POST['concentracion'];
    $adicional = $_POST['adicional'];
    $precio = $_POST['precio'];
    $marca = $_POST['marca'];
    $producto->crear($nombre, $concentracion, $adicional, $precio, $marca);
}

if ($_POST['funcion'] == 'editar') {
    $id=$_POST['id'];
    $nombre = $_POST['nombre'];
    $concentracion = $_POST['concentracion'];
    $adicional = $_POST['adicional'];
    $precio = $_POST['precio'];
    $marca = $_POST['marca'];
    $producto->editar($id,$nombre, $concentracion, $adicional, $precio, $marca);
}

if ($_POST['funcion'] == 'buscar') {
    $json = array();
    $producto->buscar();
   
    foreach ($producto->objetos as $objeto) {
       
        $json[] = array(
           
            'id' => $objeto->id_producto,
            'nombre' => $objeto->nombre,
            'concentracion' => $objeto->concentracion,
            'adicional' => $objeto->adicional,
            'precio' => $objeto->precio,
            'marca' => $objeto->marca,
            'id_marca' => $objeto->prod_marca
        );
    }

    $jsonstring = json_encode($json);
    echo $jsonstring;
}

if($_POST['funcion']=='borrar') {
    $id=$_POST['id'];
    $producto->borrar($id);
}

?>
