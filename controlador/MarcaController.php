<?php
include_once '../modelo/Marca.php';
$marca=new marca();
if($_POST['funcion']=='crear'){
    $nombre=$_POST['nombre_marca'];
    $marca->crear($nombre);
}

if($_POST['funcion']=='editar'){
    $nombre=$_POST['nombre_marca'];
    $id_editado=$_POST['id_editado'];
    $marca->editar($nombre,$id_editado);
}

if($_POST['funcion']=='buscar') {
    $json=array();
    $marca->buscar();
    foreach($marca->objetos as $objeto) {
        $json[]= array(
            'id'=>$objeto->id_marca,
            'nombre'=>$objeto->nombre    
        );
    }
    $jsonstring= json_encode($json);
    echo $jsonstring;
}



if($_POST['funcion']=='borrar') {
    $id=$_POST['id'];
    $marca->borrar($id);
}

if($_POST['funcion']=='rellenar_marcas') {
    $marca->rellenar_marcas();
    $json=array();
    foreach($marca->objetos as $objeto){
        $json[]=array(
            'id'=>$objeto->id_marca,
            'nombre'=>$objeto->nombre
        );
    }
    $jsonstring= json_encode($json);
    echo $jsonstring;
}


?>