$(document).ready(function() {
    buscar_marca();
    var funcion;
    var edit=false;
    $('#form-crear-marca').submit(e=>{
        let nombre_marca=$('#nombre-marca').val();
        let id_editado=$('#id_editar_marca').val();
        if(edit==false){
            funcion='crear';
        }else{
            funcion='editar';
        }
        
        $.post('../controlador/MarcaController.php',{nombre_marca,id_editado,funcion},(response)=>{
            if(response=='add'){
                $('#add-marca').hide('slow');
                $('#add-marca').show(1000);
                $('#add-marca').hide(2000);
                $('#form-crear-marca').trigger('reset');
                buscar_marca();
            }
            if(response=='noadd'){
                $('#noadd-marca').hide('slow');
                $('#noadd-marca').show(1000);
                $('#noadd-marca').hide(2000);
                $('#form-crear-marca').trigger('reset');
            }
            if(response=='edit'){
                $('#edit-marca').hide('slow');
                $('#edit-marca').show(1000);
                $('#edit-marca').hide(2000);
                $('#form-crear-marca').trigger('reset');
                buscar_marca();
            }
            edit=false;
            })
            e.preventDefault();
        });

        function buscar_marca(consulta) {
            funcion='buscar';
            $.post('../controlador/MarcaController.php',{consulta,funcion},(response) => {
                const marcas = JSON.parse(response);
                let template='';
                marcas.forEach(marca => {
                    template+=
                    `<tr marId="${marca.id}" marNombre="${marca.nombre}">
                            <td>
                                <button class="editar btn btn-success" title="Editar marca" type="button" data-toggle="modal" data-target="#crearmarca">
                                    <i class="fas fa-pencil-alt"></i>
                                </button>
                                <button class="borrar btn btn-danger" title="Eliminar marca">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                           
                            <td>${marca.nombre}</td>
                        </tr>
                    `;
                });
                $('#marcas').html(template);
            })
        }
        $(document).on('keyup','#buscar-marca',function(){
            let valor=$(this).val();
            if(valor!=""){
                buscar_marca(valor);
            }
            else{
                buscar_marca();
            }
        })

      

    

        $(document).on('click','.borrar',(e)=>{
            funcion='borrar';
            const elemento=$(this)[0].activeElement.parentElement.parentElement;
            const id=$(elemento).attr('marId');
            const nombre=$(elemento).attr('marNombre');
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success',
                  cancelButton: 'btn btn-danger mr-1'
                },
                buttonsStyling: false
              })
              
              swalWithBootstrapButtons.fire({
                title: 'Desea eliminar '+nombre+'?',
                text: "No podrá revertir esto!",
                icon:'warning',
                showCancelButton: true,
                confirmButtonText: 'si, borrar esto!',
                cancelButtonText: 'No, cancelar!',
                reverseButtons: true
              }).then((result) => {
                if (result.value) {
                $.post('../controlador/MarcaController.php',{id,funcion},(response) => {
                    edit=false;
                    if(response=='borrado'){
                        swalWithBootstrapButtons.fire(
                            'Borrado!',
                            'La marca '+nombre+' fue borrado.',
                            'success'
                        )
                        buscar_marca();
                    }
                    else {
                        swalWithBootstrapButtons.fire(
                            'Cancelado',
                            'La marca '+nombre+' no se pudo borrar porque está siendo usado en otro producto',
                            'error'
                        )
                        buscar_marca();
                    }
                })
                  
                  
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                  swalWithBootstrapButtons.fire(
                    'Cancelado',
                    'La marca '+nombre+' no fue borrado.',
                    'error'
                  )
                  buscar_marca();
                }
              });
          
          })

          $(document).on('click','.editar',(e)=>{
            funcion='editar';
            const elemento=$(this)[0].activeElement.parentElement.parentElement;
            const id=$(elemento).attr('marId');
            const nombre=$(elemento).attr('marNombre');
            $('#id_editar_marca').val(id);
            $('#nombre-marca').val(nombre);        
            edit=true;       
          })
    
});