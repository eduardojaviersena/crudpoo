$( document ).ready( function ()
{
  var funcion = '';
  var edit = false;
  /*$( '.select2' ).select2();*/
  rellenar_marcas();
  buscar_producto();
 



  function rellenar_marcas ()
  {
    funcion = "rellenar_marcas";
    $.post( '../controlador/MarcaController.php', { funcion }, ( response ) =>
    {
      const marcas = JSON.parse( response );
      let template = '';
      marcas.forEach( marca =>
      {
        template += `
                <option value="${marca.id }">${ marca.nombre }</option>
                `;
      } );
      $( '#marca' ).html( template );
    } )
  }



 

  $( '#form-crear-producto' ).submit( e =>
  {
    let id = $( '#id_edit_prod' ).val();
    let nombre = $( '#nombre_producto' ).val();
    let concentracion = $( '#concentracion' ).val();
    let adicional = $( '#adicional' ).val();
    let precio = $( '#precio' ).val();
    let marca = $( '#marca' ).val();
   
    if ( edit == true )
    {
      funcion = 'editar';
    } else
    {
      funcion = 'crear';
    }

    $.post( '../controlador/ProductoController.php', { funcion, id, nombre, concentracion, adicional, precio, marca}, ( response ) =>
    {
      console.log(response);

      if ( response == 'add' )
      {
        $( '#add' ).hide( 'slow' );
        $( '#add' ).show( 1000 );
        $( '#add' ).hide( 2000 );
        $( '#form-crear-producto' ).trigger( 'reset' );
        buscar_producto();
      }
      if ( response == 'edit' )
      {
        $( '#edit-prod' ).hide( 'slow' );
        $( '#edit-prod' ).show( 1000 );
        $( '#edit-prod' ).hide( 2000 );
        $( '#form-crear-producto' ).trigger( 'reset' );
        buscar_producto();
      }
      if ( response == 'noadd' )
      {
        $( '#noadd' ).hide( 'slow' );
        $( '#noadd' ).show( 1000 );
        $( '#noadd' ).hide( 2000 );
        $( '#form-crear-producto' ).trigger( 'reset' );
      }
      if ( response == 'noedit' )
      {
        $( '#noadd' ).hide( 'slow' );
        $( '#noadd' ).show( 1000 );
        $( '#noadd' ).hide( 2000 );
        $( '#form-crear-producto' ).trigger( 'reset' );
      }
      edit = false;
    } );

    e.preventDefault();
  } );

  function buscar_producto ( consulta )
  {
    funcion = "buscar";
    $.post( '../controlador/ProductoController.php', { consulta, funcion }, ( response ) =>
    {
        console.log(response);
      const productos = JSON.parse( response );
     
      let template = '';
      productos.forEach( producto =>
      {
        template += `

                         <tr prodId="${producto.id }"  prodNombre="${ producto.nombre }" prodConcentracion="${ producto.concentracion }" prodAdicional="${ producto.adicional }" prodPrecio="${ producto.precio }" prodMarca="${ producto.marca }" idMarca="${ producto.id_marca }"  >
                            <td>
                                <button class="editar-prod btn btn-success" title="Editar producto" type="button" data-toggle="modal" data-target="#crearproducto">
                                    <i class="fas fa-pencil-alt"></i>
                                </button>
                                <button class="borrar-prod btn btn-danger" title="Eliminar producto">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                           
                            <td>${ producto.nombre }</td>
                            <td>${ producto.concentracion }</td>
                            <td>${ producto.adicional }</td>
                            <td>${ producto.precio }</td>
                            <td>${ producto.marca }</td>
                        </tr>
            
                `;
      } );
      $( '#productos' ).html( template );
    } )
  }



  $( document ).on( 'keyup', '#buscar-producto', function ()
  {
    let valor = $( this ).val();
    if ( valor != "" )
    {
      buscar_producto( valor );
    }
    else
    {
      buscar_producto();
    }
  } );


  $( document ).on( 'click', '.editar-prod', ( e ) =>
  {
    const elemento = $( this )[ 0 ].activeElement.parentElement.parentElement;
    const id = $( elemento ).attr( 'prodId' );
    const nombre = $( elemento ).attr( 'prodNombre' );
    const concentracion = $( elemento ).attr( 'prodConcentracion' );
    const adicional = $( elemento ).attr( 'prodAdicional' );
    const precio = $( elemento ).attr( 'prodPrecio' );
    const marca = $( elemento ).attr( 'idMarca' );


    $( '#id_edit_prod' ).val( id );
    $( '#nombre_producto' ).val( nombre );
    $( '#concentracion' ).val( concentracion );
    $( '#adicional' ).val( adicional );
    $( '#precio' ).val( precio );
    $( '#marca' ).val( marca ).trigger( 'change' );
   
    edit = true;
  } );

  $(document).on('click','.borrar-prod',(e)=>{
    funcion='borrar';
    const elemento=$(this)[0].activeElement.parentElement.parentElement;
    const id=$(elemento).attr('prodId');
    const nombre=$(elemento).attr('prodNombre');
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger mr-1'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Desea eliminar '+nombre+'?',
        text: "No podrá revertir esto!",
        icon:'warning',
        showCancelButton: true,
        confirmButtonText: 'si, borrar esto!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
        $.post('../controlador/ProductoController.php',{id,funcion},(response) => {
            edit=false;
            if(response=='borrado'){
                swalWithBootstrapButtons.fire(
                    'Borrado!',
                    'El producto '+nombre+' fue borrado.',
                    'success'
                )
                buscar_producto();
            }
            else {
                swalWithBootstrapButtons.fire(
                    'Cancelado',
                    'El producto '+nombre+' no se pudo borrar porque está siendo usado en otro lote',
                    'error'
                )
            }
        })
          
          
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire(
            'Cancelado',
            'El producto '+nombre+' no fue borrado.',
            'error'
          )
        }
      });
  
  });



} );
