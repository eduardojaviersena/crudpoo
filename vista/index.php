<?php

include_once 'layouts/header.php';
?>

<title>Adm | Producto</title>
<?php
include_once 'layouts/nav.php';
?>


<!-- Modal -->
<div class="modal fade" id="crearproducto" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="card card-success">
        <div class="card-header">
          <h3 class="card-title">Crear producto</h3>
          <button type="button" data-dismiss="modal" aria-label="close" class="close ">
            <span aria-hidden="true">&times;</span>
          </button>
          </h3>
        </div>
        <div class="card-body">
          <div class="alert alert-success text-center" id="add" style="display:none;">
            <span><i class="fas fa-check m-1"></i>SE agregó correctamente</span>
          </div>
          <div class="alert alert-danger text-center" id="noadd" style="display:none;">
            <span><i class="fas fa-times m-1"></i>El producto ya existe</span>
          </div>
          <div class="alert alert-success text-center" id="edit-prod" style="display:none;">
            <span><i class="fas fa-check m-1"></i>SE editó correctamente</span>
          </div>
          <form id="form-crear-producto">
            <div class="form-group">
              <label for="nombre_producto">Nombre</label>
              <input id="nombre_producto" type="text" class="form-control" placeholder="Ingrese nombre" required>
            </div>
            <div class="form-group">
              <label for="concentracion">Concentracion</label>
              <input id="concentracion" type="text" class="form-control" placeholder="Ingrese concentracion">
            </div>
            <div class="form-group">
              <label for="adicional">Adicional</label>
              <input id="adicional" type="text" class="form-control" placeholder="Ingrese adicional">
            </div>
            <div class="form-group">
              <label for="precio">Precio</label>
              <input id="precio" type="number" step="any" value="1" class="form-control" placeholder="Ingrese precio" required>
            </div>

            <div class="form-group">
              <label for="marca">Marca</label>
              <select name="marca" id="marca" class="form-control select2" style="width: 100%"></select>
            </div>


            <input type="hidden" id="id_edit_prod">
        </div>
        <div class="card-footer">
          <button type="submit" class="btn bg-gradient-primary float-right m-1">Guardar</button>
          <button type="button" data-dismiss="modal" class="btn btn-outline-secondary float-right m-1">Cerrar</button>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="crearmarca" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="card card-success">
        <div class="card-header">
          <h3 class="card-title">Crear marca</h3>
          <button type="button" data-dismiss="modal" aria-label="close" class="close ">
            <span aria-hidden="true">&times;</span>
          </button>
          </h3>
        </div>
        <div class="card-body">
          <div class="alert alert-success text-center" id="add-marca" style="display:none;">
            <span><i class="fas fa-check m-1"></i>SE agregó correctamente</span>
          </div>
          <div class="alert alert-danger text-center" id="noadd-marca" style="display:none;">
            <span><i class="fas fa-times m-1"></i>La marca ya existe</span>
          </div>
          <div class="alert alert-success text-center" id="edit-marca" style="display:none;">
            <span><i class="fas fa-check m-1"></i>SE editó correctamente</span>
          </div>
          <form id="form-crear-marca">
            <div class="form-group">
              <label for="nombre-marca">Nombre</label>
              <input id="nombre-marca" type="text" class="form-control" placeholder="Ingrese nombre" required>
              <input type="hidden" id="id_editar_marca">
            </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn bg-gradient-primary float-right m-1">Guardar</button>
          <button type="button" data-dismiss="modal" class="btn btn-outline-secondary float-right m-1">Cerrar</button>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>



<!-- Content Wrapper. Contains page content -->
<div class="content">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Gestion producto</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Gestion producto</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <ul class="nav nav-pills">
                <li class="nav-item"><a href="#marcaprod" class="nav-link active" data-toggle="tab">Marca</a></li>
                <li class="nav-item"><a href="#producto" class="nav-link" data-toggle="tab">Producto</a></li>
              </ul>
            </div>
            <div class="card-body p-0">
              <div class="tab-content">
                <div class="tab-pane active" id='marcaprod'>
                  <div class="card card-success">
                    <div class="card-header">
                      <div class="card-title">Buscar marca <button type="button" data-toggle="modal" data-target="#crearmarca" class="btn bg-gradient-primary btn-sm m-2">Crear marca</button></div>
                      <div class="input-group">
                        <input id="buscar-marca" type="text" class="form-control float-left" placeholder="Ingrese nombre">
                        <div class="input-group-append">
                          <button class="btn btn-default"><i class="fas fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-hover text-nowrap">
                        <thead class="table-success">
                          <tr>
                            <th>Acción</th>

                            <th>Marca</th>


                          </tr>
                        </thead>
                        <tbody class="table-active" id="marcas">

                        </tbody>
                      </table>
                    </div>
                    <div class="card-footer">

                    </div>
                  </div>
                </div>
                <div class="tab-pane" id='producto'>
                  <div class="card card-success">
                    <div class="card-header">
                      <div class="card-title">Buscar producto<button type="button" data-toggle="modal" data-target="#crearproducto" class="btn bg-gradient-primary btn-sm m-2">Crear producto</button></div>
                      <div class="input-group">
                        <input id="buscar-producto" type="text" class="form-control float-left" placeholder="Ingrese nombre">
                        <div class="input-group-append">
                          <button class="btn btn-default"><i class="fas fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-hover text-nowrap">
                        <thead class="table-success">
                          <tr>
                            <th>Acción</th>
                            <th>Nombre</th>
                            <th>Concentracion</th>
                            <th>Adicional</th>
                            <th>Precio</th>
                            <th>Marca</th>
                          </tr>
                        </thead>
                        <tbody class="table-active" id="productos">

                        </tbody>
                      </table>
                    </div>
                    <div class="card-footer">

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card footer">

            </div>
          </div>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
include_once 'layouts/footer.php';
?>
<script src="../js/Marca.js"></script>

<script src="../js/Producto.js"></script>